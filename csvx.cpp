#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "include/csvx.hpp"

SCENARIO("When a mock quoted '\"' and comma delimeted CSV string with a header is parsed it is correct.",
         "[quoted_comma_csv_string]") {
  GIVEN("A mocked CSV string and a mocked CSV object.") {
    constexpr auto mock_csv_string_a = R"("Name","Address","Balance",
"Cody Wink", "1234 Foobar Ave, Orlando, FL","$124.00",
"John Doe", "124 Regan Ave, Sanford, FL","$99.00",
"Sally Hay", "678 Something Rd, Jacksonville, FL","$821.00",
"Bob Ross", "888 Hecklo Dr, Maitland, FL","$882.00",
)";

    auto csv_b = csvx::CSV({{"Cody Wink", "1234 Foobar Ave, Orlando, FL", "$124.00"},
                            {"John Doe", "124 Regan Ave, Sanford, FL", "$99.00"},
                            {"Sally Hay", "678 Something Rd, Jacksonville, FL", "$821.00"},
                            {"Bob Ross", "888 Hecklo Dr, Maitland, FL", "$882.00"}});

    WHEN("The mocked string is parsed.") {
      auto csv_a = csvx::parse_string(mock_csv_string_a, ',', '"', true);

      THEN("The mocked string parsed is equal to the mocked CSV object.") { REQUIRE(csv_a == csv_b); }
    }
  }
}

SCENARIO("When a mock quoted '\"' and pipe delimeted CSV string with a header is parsed it is correct.",
         "[quoted_pipe_csv_string]") {
  GIVEN("A mocked CSV string and a mocked CSV object.") {
    constexpr auto mock_csv_string_a = R"("Name"|"Address"|"Balance"|
"Cody Wink"| "1234 Foobar Ave, Orlando, FL"|"$124.00"|
"John Doe"| "124 Regan Ave, Sanford, FL"|"$99.00"|
"Sally Hay"| "678 Something Rd, Jacksonville, FL"|"$821.00"|
"Bob Ross"| "888 Hecklo Dr, Maitland, FL"|"$882.00"|
)";

    auto csv_b = csvx::CSV({{"Cody Wink", "1234 Foobar Ave, Orlando, FL", "$124.00"},
                            {"John Doe", "124 Regan Ave, Sanford, FL", "$99.00"},
                            {"Sally Hay", "678 Something Rd, Jacksonville, FL", "$821.00"},
                            {"Bob Ross", "888 Hecklo Dr, Maitland, FL", "$882.00"}});

    WHEN("The mocked string is parsed.") {
      auto csv_a = csvx::parse_string(mock_csv_string_a, '|', '"', true);

      THEN("The mocked string parsed is equal to the mocked CSV object.") { REQUIRE(csv_a == csv_b); }
    }
  }
}

SCENARIO("When a mock non-quoted and pipe delimeted CSV string with a header is parsed it is correct.",
         "[nonquoted_pipe_csv_string]") {
  GIVEN("A mocked CSV string and a mocked CSV object.") {
    constexpr auto mock_csv_string_a = R"(Name|Address|Balance|
Cody Wink|1234 Foobar Ave, Orlando, FL|$124.00|
John Doe|124 Regan Ave, Sanford, FL|$99.00|
Sally Hay|678 Something Rd, Jacksonville, FL|$821.00|
Bob Ross|888 Hecklo Dr, Maitland, FL|$882.00|
)";

    auto csv_b = csvx::CSV({{"Cody Wink", "1234 Foobar Ave, Orlando, FL", "$124.00"},
                            {"John Doe", "124 Regan Ave, Sanford, FL", "$99.00"},
                            {"Sally Hay", "678 Something Rd, Jacksonville, FL", "$821.00"},
                            {"Bob Ross", "888 Hecklo Dr, Maitland, FL", "$882.00"}});

    WHEN("The mocked string is parsed.") {
      auto csv_a = csvx::parse_string(mock_csv_string_a, '|', '\0', true);

      THEN("The mocked string parsed is equal to the mocked CSV object.") { REQUIRE(csv_a == csv_b); }
    }
  }
}

SCENARIO("When a mock non-quoted and pipe delimeted CSV string without a header is parsed it is correct.",
         "[nonquoted_pipe_noheader_csv_string]") {
  GIVEN("A mocked CSV string and a mocked CSV object.") {
    constexpr auto mock_csv_string_a = R"(Cody Wink|1234 Foobar Ave, Orlando, FL|$124.00|
John Doe|124 Regan Ave, Sanford, FL|$99.00|
Sally Hay|678 Something Rd, Jacksonville, FL|$821.00|
Bob Ross|888 Hecklo Dr, Maitland, FL|$882.00|
)";

    auto csv_b = csvx::CSV({{"Cody Wink", "1234 Foobar Ave, Orlando, FL", "$124.00"},
                            {"John Doe", "124 Regan Ave, Sanford, FL", "$99.00"},
                            {"Sally Hay", "678 Something Rd, Jacksonville, FL", "$821.00"},
                            {"Bob Ross", "888 Hecklo Dr, Maitland, FL", "$882.00"}});

    WHEN("The mocked string is parsed.") {
      auto csv_a = csvx::parse_string(mock_csv_string_a, '|', '\0', false);

      THEN("The mocked string parsed is equal to the mocked CSV object.") { REQUIRE(csv_a == csv_b); }
    }
  }
}

SCENARIO("When a CSV object is converted to a string, the output will match the correct string.", "[csv_string]") {
  GIVEN("A mocked CSV string and a mocked CSV object.") {
    constexpr auto mock_csv_string_a = R"(Cody Wink|1234 Foobar Ave, Orlando, FL|$124.00|
John Doe|124 Regan Ave, Sanford, FL|$99.00|
Sally Hay|678 Something Rd, Jacksonville, FL|$821.00|
Bob Ross|888 Hecklo Dr, Maitland, FL|$882.00|
)";

    auto csv_b = csvx::CSV({{"Cody Wink", "1234 Foobar Ave, Orlando, FL", "$124.00"},
                            {"John Doe", "124 Regan Ave, Sanford, FL", "$99.00"},
                            {"Sally Hay", "678 Something Rd, Jacksonville, FL", "$821.00"},
                            {"Bob Ross", "888 Hecklo Dr, Maitland, FL", "$882.00"}});

    WHEN("The string is created.") {
      auto mock_csv_string_b = csvx::string(csv_b, '|', '\0');

      THEN("The string will match the correct output.") { REQUIRE(mock_csv_string_a == mock_csv_string_b); }
    }
  }
}

SCENARIO("When a CSV object is converted to a string with quotes, the output will match the correct string.",
         "[csv_string_quote]") {
  GIVEN("A mocked CSV string and a mocked CSV object.") {
    constexpr auto mock_csv_string_a = R"("Cody Wink"|"1234 Foobar Ave, Orlando, FL"|"$124.00"|
"John Doe"|"124 Regan Ave, Sanford, FL"|"$99.00"|
"Sally Hay"|"678 Something Rd, Jacksonville, FL"|"$821.00"|
"Bob Ross"|"888 Hecklo Dr, Maitland, FL"|"$882.00"|
)";

    auto csv_b = csvx::CSV({{"Cody Wink", "1234 Foobar Ave, Orlando, FL", "$124.00"},
                            {"John Doe", "124 Regan Ave, Sanford, FL", "$99.00"},
                            {"Sally Hay", "678 Something Rd, Jacksonville, FL", "$821.00"},
                            {"Bob Ross", "888 Hecklo Dr, Maitland, FL", "$882.00"}});

    WHEN("The string is created.") {
      auto mock_csv_string_b = csvx::string(csv_b, '|', '"');

      THEN("The string will match the correct output.") { REQUIRE(mock_csv_string_a == mock_csv_string_b); }
    }
  }
}

SCENARIO("When a CSV object is written to a file it is read back in the same form.", "[csv_read_write]") {
  GIVEN("A mocked CSV object.") {
    auto csv_a = csvx::CSV({{"Name", "Address", "Balance"},
                            {"Cody Wink", "1234 Foobar Ave, Orlando, FL", "$124.00"},
                            {"John Doe", "124 Regan Ave, Sanford, FL", "$99.00"},
                            {"Sally Hay", "678 Something Rd, Jacksonville, FL", "$821.00"},
                            {"Bob Ross", "888 Hecklo Dr, Maitland, FL", "$882.00"}});

    WHEN("The file is written and read.") {
      csvx::write_file(csv_a, "csv_read_write.csv", '|', '"');

      auto csv_b = csvx::parse_file("csv_read_write.csv", '|', '"', false);

      THEN("The read object will match the original object.") { REQUIRE(csv_a == csv_b); }
    }
  }
}

SCENARIO(
    "When a mock csv string using spaces for delimeter, no header and no quotes is parsed, the results are correct.",
    "[space_no_quote_parsed]") {
  GIVEN("A mocked CSV string.") {
    constexpr auto mock_csv_string_a = R"(nxkmwucvtd 86.24.25.217 jilspe.nxkmwucvtd
pbyfvsljwm 166.156.44.186 engkia.pbyfvsljwm
ybspkvzgra 229.102.56.255 oqujmn.ybspkvzgra)";

    auto csv_a = csvx::CSV({{"nxkmwucvtd", "86.24.25.217", "jilspe.nxkmwucvtd"},
                            {"pbyfvsljwm", "166.156.44.186", "engkia.pbyfvsljwm"},
                            {"ybspkvzgra", "229.102.56.255", "oqujmn.ybspkvzgra"}});

    WHEN("The mocked csv string is parsed.") {
      auto csv_b = csvx::parse_string(mock_csv_string_a, ' ', '\0', false);

      THEN("The result is correct.") { REQUIRE(csv_a == csv_b); }
    }
  }
}

SCENARIO("When a mock csv string using spaces for delimeter, no header and quotes is parsed, the results are correct.",
         "[space_quote_parsed]") {
  GIVEN("A mocked CSV string.") {
    constexpr auto mock_csv_string_a = R"("nxkmwucvtd" "86.24.25.217" "jilspe.nxkmwucvtd"
"pbyfvsljwm" "166.156.44.186" "engkia.pbyfvsljwm"
"ybspkvzgra" "229.102.56.255" "oqujmn.ybspkvzgra")";

    auto csv_a = csvx::CSV({{"nxkmwucvtd", "86.24.25.217", "jilspe.nxkmwucvtd"},
                            {"pbyfvsljwm", "166.156.44.186", "engkia.pbyfvsljwm"},
                            {"ybspkvzgra", "229.102.56.255", "oqujmn.ybspkvzgra"}});

    WHEN("The mocked csv string is parsed.") {
      auto csv_b = csvx::parse_string(mock_csv_string_a, ' ', '"', false);

      THEN("The result is correct.") { REQUIRE(csv_a == csv_b); }
    }
  }
}
