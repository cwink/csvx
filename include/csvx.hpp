#ifndef CSVX_HPP
#define CSVX_HPP

#include <stdexcept>
#include <string>
#include <vector>

namespace csvx {
using String = std::string;
using Record = std::vector<String>;
using CSV = std::vector<Record>;
using Error = std::runtime_error;

auto parse_string(const std::string &string, char delimeter, char quote, bool has_header) -> CSV;
auto parse_file(const std::string &file, char delimeter, char quote, bool has_header) -> CSV;
auto string(const CSV &csv, char delimeter, char quote) -> std::string;
auto write_file(const CSV &csv, const std::string &file, char delimeter, char quote) -> void;
} // namespace csvx

#endif // CSVX_HPP
