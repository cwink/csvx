#include "../include/csvx.hpp"

#include <fstream>
#include <sstream>

namespace {
auto parse(std::istream &stream, const char delimeter, const char quote, const bool has_header) -> csvx::CSV {
  if (has_header) {
    auto line = std::string("");

    std::getline(stream, line);
  }

  auto del = delimeter == '\0' ? ',' : delimeter;
  auto csv = csvx::CSV();

  if (quote != '\0') {
    for (auto line = std::string(""); std::getline(stream, line);) {
      auto ss = std::istringstream(line);
      auto record = csvx::Record();

      while (ss.peek() != -1) {
        auto string_stream = std::ostringstream();

        while (std::isspace(del) == 0 && std::isspace(ss.peek()) != 0) {
          ss.ignore();
        }

        if (ss.peek() != quote) {
          throw csvx::Error("Expected '" + std::string(1, quote) + "' but found '" +
                            std::string(1, static_cast<char>(ss.peek())) + "'.");
        }

        ss.ignore();

        while (ss.peek() != quote) {
          string_stream << static_cast<char>(ss.get());
        }

        ss.ignore();

        while (std::isspace(del) == 0 && std::isspace(ss.peek()) != 0) {
          ss.ignore();
        }

        if (ss.peek() != del && ss.peek() != -1) {
          throw csvx::Error("Expected '" + std::string(1, del) + "' but found '" +
                            std::string(1, static_cast<char>(ss.peek())) + "'.");
        }

        ss.ignore();

        record.emplace_back(string_stream.str());
      }

      csv.emplace_back(record);
    }
  } else {
    for (auto line = std::string(""); std::getline(stream, line);) {
      auto ss = std::istringstream(line);
      auto record = csvx::Record();

      while (ss.peek() != -1) {
        auto string_stream = std::ostringstream();

        while (ss.peek() != del && ss.peek() != -1) {
          string_stream << static_cast<char>(ss.get());
        }

        ss.ignore();

        record.emplace_back(string_stream.str());
      }

      csv.emplace_back(record);
    }
  }

  return csv;
}
} // namespace

namespace csvx {
auto parse_string(const std::string &string, const char delimeter, const char quote, const bool has_header) -> CSV {
  auto stream = std::istringstream(string);

  return parse(stream, delimeter, quote, has_header);
}

auto parse_file(const std::string &file, const char delimeter, const char quote, const bool has_header) -> CSV {
  auto file_stream = std::ifstream(file);

  if (!file_stream) {
    throw Error("Unable to open file '" + file + "'.");
  }

  return parse(file_stream, delimeter, quote, has_header);
}

auto string(const CSV &csv, const char delimeter, const char quote) -> std::string {
  auto del = delimeter == '\0' ? ',' : delimeter;
  auto string_stream = std::ostringstream();

  for (auto record = std::cbegin(csv); record != std::cend(csv); ++record) {
    for (auto cell = std::cbegin(*record); cell != std::cend(*record); ++cell) {
      if (quote != '\0') {
        string_stream << quote;
      }

      string_stream << *cell;

      if (quote != '\0') {
        string_stream << quote;
      }

      string_stream << del;
    }

    string_stream << std::endl;
  }

  return string_stream.str();
}

auto write_file(const CSV &csv, const std::string &file, const char delimeter, const char quote) -> void {
  auto file_stream = std::ofstream(file);

  if (!file_stream) {
    throw Error("Unable to open file '" + file + "'.");
  }

  file_stream << string(csv, delimeter, quote);
}
} // namespace csvx
